//
//  CocoaDemoFile.swift
//  CocoaDemo
//
//  Created by wasim-zstk238 on 12/07/22.
//

import Foundation

public class CocoaDemoFile {
     
    public init() {}
    public func add(_ a:Int, _ b:Int) -> Int {
        return a + b
    }
    public func sub(_ a:Int, _ b:Int) -> Int {
        return a - b
    }
}
